# Tripillar dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information and colour themes](http://dokuwiki.org/template:tripillar)

![tripillar theme light screenshot](https://i.imgur.com/GBtn4lb.png)

![tripillar theme dark screenshot](https://i.imgur.com/fLbvTpF.png)